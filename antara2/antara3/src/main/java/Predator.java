public abstract class Predator extends Animal {
    @Override
    protected boolean IsGoodFood(Food food) {
        return food.getClass().getName().equals(PredatorFood.class.getName());
    }

    public int cute;
}
