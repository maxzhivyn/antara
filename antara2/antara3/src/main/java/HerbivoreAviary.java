public class HerbivoreAviary extends Aviary {
    public HerbivoreAviary(int size) {
        super(size);
    }

    @Override
    protected boolean isGoodAnimal(Animal animal) {
        return Herbivore.class.isAssignableFrom(animal.getClass());
    }
}
