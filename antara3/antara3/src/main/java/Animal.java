public abstract class Animal {
    public void Eat(Food food) throws Exception {
        if (!IsGoodFood(food)) {
            System.out.println(this.toString() + "нелья накормить " + food.toString());
            return;
        }

        System.out.println(this.toString() + " поел(а) и сказал(а) " + MakeVoice());
    }

    protected abstract String MakeVoice();
    protected abstract boolean IsGoodFood(Food food);
}
